from cli import parse_args
from split_processor import SplitProcessor


def main():

    args = parse_args()
    split_processor = SplitProcessor(
        args.file_path,
        args.lines,
        args.bytes
    )

    split_processor.run()


if __name__ == '__main__':
    main()
