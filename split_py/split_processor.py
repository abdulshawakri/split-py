import re
from typing import Optional


class SplitProcessor:

    def __init__(self, file_path: str, split_lines: Optional[int], split_bytes: Optional[int]):
        self._file_path: str = file_path
        self._split_lines: int = split_lines
        self._split_bytes: int = split_bytes

        if not self._file_path:
            raise ValueError(f"No file passed to be processed '{self._file_path}', please pass a file path.")

        # TODO: Create custom exceptions
        if not (self._split_bytes or self._split_lines):
            raise ValueError("Lines or Bytes must be passed to process the file.")

        if self._split_lines and self._split_bytes:
            raise ValueError("Cannot process both bytes and lines together at the moment.")

    def run(self):
        if self._split_lines:
            self._split_by_lines()
        else:
            self._split_by_bytes()

    def _split_by_lines(self):

        with open(self._file_path, 'r') as input_file:
            file_lines = input_file.readlines()

            if self._has_only_line_breaks(file_lines[-1]):
                file_lines.pop()

            num_files = self._calculate_number_of_files(len(file_lines), self._split_lines)

            for i in range(num_files):
                file_name = f'{self._file_path}_part_{i}'
                with open(file_name, 'w') as output_file:
                    output_file.writelines(file_lines[i*self._split_lines:(i+1)*self._split_lines])

    def _split_by_bytes(self):
        # TODO: Handle suffixes with "B", "KB" etc.. for now KISS
        with open(self._file_path, 'rb') as input_file:
            file_bytes = input_file.read()

            num_files = self._calculate_number_of_files(len(file_bytes), self._split_bytes)

            for i in range(num_files):
                file_name = f'{self._file_path}_part_{i}'
                with open(file_name, 'wb') as output_file:
                    output_file.write(file_bytes[i*self._split_bytes:(i+1)*self._split_bytes])

    @staticmethod
    def _has_only_line_breaks(line: str) -> bool:
        return bool(re.match(r'^[\r\n]+$', line))

    @staticmethod
    def _calculate_number_of_files(length: int, split_param: int) -> int:
        if length % split_param > 0:
            return length // split_param + 1
        return length // split_param
