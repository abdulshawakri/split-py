# Split-PY

Is a python package to semi-replace the unix `split` command.

## Maintainers

Abdulrahman Alshawakri <[abdulrahman.alshawakri@gmail.com](mailto:abdulrahman.alshawakri@gmail.com)>

## Developers

Abdulrahman Alshawakri <[abdulrahman.alshawakri@gmail.com](mailto:abdulrahman.alshawakri@gmail.com)>

## Project State

Prototype

## Project Type

Python 3.10

## Setup

### PyEnv installation
* Install `pyenv` following the instruction https://github.com/pyenv/pyenv
    * `pyenv` is python version management system to easily work in different python versions
* Execute ```pyenv install 3.10.0```
* In the current directory, set the python version. This creates the file .python-version.
  ```pyenv local 3.10.0```
* Validate via ```pyenv version```

### Poetry installation

Poetry is a packaging and dependency manager for python, and it's superior to `pipenv, conda and virtualvenv`

* Follow the instruction in here https://python-poetry.org/docs/#installation
* Configure poetry to create virtual app.environments inside the project's root directory
  ```poetry config virtualenvs.in-project true```
* Create a virtual environment via ```poetry install```

## Usage

* Activate the environment:

    ```source .venv/bin/activate```
* 
* Execute `python split_py/main.py <file_path> -l <lines_to_split>` to split by lines
* Execute `python split_py/main.py <file_path> -b <bytes_to_split>` to split by bytes


## Build and Install


```shell
poetry build # Will generate a dist/ file
pip3 install dist/split_py-0.1.0-py3-none-any.whl #  <package_name>-<version>-py3-none-any.whl
```

## Run Tests

To run all tests, execute:

    pytest split_py/ --cov


## TODOs

- [ ] Create unit-tests for the CLI.
- [ ] Create a .gitlab-ci.yaml to automate the CI pipeline.
- [ ] For bytes consider the suffixes `KB, MB, GB, etc..`
- [ ] Look into further enhancements like using `multiprocessing` and `buffer the I/O` for reading and writing.
