import os
import tempfile

import pytest

from split_py.split_processor import SplitProcessor


@pytest.fixture
def test_file() -> tempfile.NamedTemporaryFile:
    # The following string contains 26 Bytes * 1000
    test_data = b'Welcome to Python Split!\n' * 1000
    test_file = tempfile.NamedTemporaryFile(delete=False)  # to persist the file to be manually deleted
    test_file.write(test_data)
    test_file.close()

    return test_file


def get_files_after_processing(test_file_path: str) -> tuple[list, str]:
    dir_path = os.path.dirname(test_file_path)
    return os.listdir(dir_path), dir_path


def tear_down(output_files: list[str]):
    for output_file in output_files:
        os.remove(output_file)


def test_split_file_by_bytes(test_file):

    output_files = []
    try:
        split_processor = SplitProcessor(test_file.name, split_lines=None, split_bytes=1000)
        split_processor.run()

        files, directory = get_files_after_processing(test_file.name)

        file_name = os.path.basename(test_file.name)
        output_files = [os.path.join(directory, part) for part in files if file_name in part]

        assert 25 == len(output_files) - 1

    finally:
        tear_down(output_files)


def test_split_file_by_lines(test_file):

    output_files = []
    try:
        split_processor = SplitProcessor(test_file.name, split_lines=100, split_bytes=None)
        split_processor.run()

        files, directory = get_files_after_processing(test_file.name)

        file_name = os.path.basename(test_file.name)
        output_files = [os.path.join(directory, part) for part in files if file_name in part]

        assert 10 == len(output_files) - 1

    finally:
        tear_down(output_files)


def test_missing_file():

    with pytest.raises(ValueError):
        split_processor = SplitProcessor("", split_lines=100, split_bytes=None)
        split_processor.run()


def test_missing_lines_and_bytes():

    with pytest.raises(ValueError):
        split_processor = SplitProcessor("test_file.txt", split_lines=None, split_bytes=None)
        split_processor.run()


def test_having_lines_and_bytes():

    with pytest.raises(ValueError):
        split_processor = SplitProcessor("test_file.txt", split_lines=100, split_bytes=100)
        split_processor.run()
