import argparse


# TODO: Create unit-tests for the CLI
def parse_args() -> argparse.Namespace:
    parser = argparse.ArgumentParser(description='Split a file into smaller files.')
    parser.add_argument('file_path', help='The path to the file to split.')

    group = parser.add_mutually_exclusive_group(required=True)

    group.add_argument('-l', '--lines', type=int, help='The number of lines per file.')
    group.add_argument('-b', '--bytes', type=int, help='The number of bytes per file.')

    return parser.parse_args()
